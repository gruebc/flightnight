import { test } from 'qunit';
import moduleForAcceptance from 'flight-night/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | test1');

test('visiting /test1', function(assert) {
  visit('/test1');

  andThen(function() {
    assert.equal(currentURL(), '/test1');
  });
});
